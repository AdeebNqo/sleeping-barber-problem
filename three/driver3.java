import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;
class driver3{
	
	static int sleep_time;
	static int num_prod;
	static int num_cons;

	static int buffer_size;

	static unique_random rand;
	static monitor mon;

	public static void main(String[] args){
		try{

			//cmd line args
			sleep_time = Integer.parseInt(args[0]);
			num_prod = Integer.parseInt(args[1]);
			num_cons = Integer.parseInt(args[2]);
			
			//init
			buffer_size = 11;
			rand = new unique_random(num_prod);
			mon = new monitor(buffer_size);

			for (int i=0;i<num_prod;i++){
				int name = rand.nextInt();
				producer prod = new producer(name);
				prod.start();
			}
			for (int i=0;i<num_cons;i++){
				consumer cons = new consumer();
				cons.start();
			}
		
			//exiting
			Thread.sleep(sleep_time);
			System.exit(0);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	static class semaphore{
		int var;
		public semaphore(int initial_value){
			this.var = initial_value;
		}
		public synchronized void _wait(){
			if (var<=0){
				try{
					wait();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			--var;
		}
		public synchronized void _signal(){
			++var;
			notify();
		}
	}
	static class monitor{
		waiting_room buffer;
		int count=0;

		semaphore lock = new semaphore(1);

		Object free_space = new Object();
		Object full_space = new Object();

		public monitor(int buffer_size){
			buffer = new waiting_room(buffer_size);
		}
		public void insert_person(int name){
			lock._wait();
			if (count>=buffer_size){
				try{
					synchronized(free_space){
						lock._signal();
						free_space.wait();
						lock._wait();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			buffer.insert_person(name);
			++count;
			synchronized(full_space){
				full_space.notify();
			}
			lock._signal();
		}
		public int remove_person(){
			lock._wait();
			if (count==0){
				try{
					synchronized(full_space){
						lock._signal();
						full_space.wait();
						lock.wait();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			int rem;
			rem = buffer.remove_person();
			--count;

			synchronized(free_space){
				free_space.notify();
			}
			lock._signal();
			return rem;
		}
		public synchronized int get_total(){
			return buffer.total;
		}
	}
	static class waiting_room{
		int[] buffer;
		int curr_pos =0;
		int total;
		int remove=0;
		int insert=0;
		public waiting_room(int n){
			buffer = new int[n];
		}
        	//methods for inserting and removing people from the queue
        	public void insert_person(int name){
        	        System.out.println("Customer "+name+" enteres queue");
			buffer[insert%buffer.length] = name;
			++insert;

        	}
        	public int remove_person(){
			++total;
			int pos = remove%buffer.length;
			int removed_name = buffer[pos];
			buffer[pos] =0;
			++remove;

			return removed_name;
        	}
        	//shifts all the elements of the buffer to the left -- once
        	public synchronized void shift_left(int[] some_buffer){
        	        for (int i=0;i<buffer.length;i++){
        	                if (i>0){
        	                        buffer[i-1] = buffer[i];
        	                }
        	                if (i>curr_pos){
        	                        buffer[i] = 0;
        	                }
        	        }
        	}
	}
	//producer
	static class producer extends Thread{
		int name;
		public producer(int name){
			this.name = name;
		}
		public void run(){
			mon.insert_person(name);
		}
	}
	static class consumer extends Thread{
		public void run(){
			do{
				System.out.println("Assisting customer "+mon.remove_person());
			}
			while(mon.get_total()<=num_prod);
		}
	}
	static class unique_random{
	int range;
	Stack<Integer> stack;
	Random rand;
	public unique_random(int n){
		range = n;
		stack = new Stack<Integer>();
		rand = new Random();
		start();
	}
	public void start(){
		for (;stack.size()<=range;){
			Integer curr_int = rand.nextInt(3*range);
			//if next generated num has not been generated before
			if (stack.search(curr_int)==-1){
				stack.push(curr_int);
			}
		}
	}
	public int nextInt(){
		return stack.pop();
	}
}
}
