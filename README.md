
Sleeping barber problem
========================

> assignment 2

> csc3002f

> operating systems

> mhlzol004(Zola Mahlaza <mailto:adeebnqo@gmail.com>)

Contents of tarball.
--------------------
Folders:

- one
- two
- three
- four

Folder 'n' represents the solution to question 'n'.
In each folder, you will find a makefile and file named driverN.java
where N will be replace by an natural number.

Compilation and more
---------------------

The rules are simple.

To compile simpy enter `make`. To run type `make run` and to clean .class files: type `make clean`

Output
-------

The program will monitor buffer activity through print statements. Here's a sample from driver1.java

		Customer 34 enteres queue
		Customer 20 enteres queue
		Customer 26 enteres queue
		Customer 8 enteres queue
		Customer 24 enteres queue
		Customer 33 enteres queue
		Customer 18 enteres queue
		Customer 9 enteres queue
		Customer 4 enteres queue
		Customer 29 enteres queue
		Customer 6 enteres queue
		Assisting customer 34
		Assisting customer 20
		Customer 32 enteres queue
		Assisting customer 26
		Assisting customer 8
		Assisting customer 24
		Assisting customer 33
		Assisting customer 18
		Assisting customer 9
		Assisting customer 4
		Assisting customer 29
		Assisting customer 6
		Assisting customer 32

To pass args to driverN.java, change the args in the makefile 
